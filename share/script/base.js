
/* browser & os check */
var ua = navigator.userAgent.toUpperCase();
var apver = navigator.appVersion.toUpperCase();
var apnm = navigator.appName.toUpperCase();

var mac = apver.indexOf("MAC",0) >= 0;
var windows = apver.indexOf("WIN",0) >= 0;
//var xp = ua.match(/NT 5\.1|XP/);

if (typeof document.documentElement.style.msInterpolationMode != "undefined") {
  // IE 7 or newer 
  var IE7 = true;
}
var ie  = apnm.indexOf("MICROSOFT INTERNET EXPLORER",0) >= 0;
var nn  = apnm.indexOf("NETSCAPE",0) >= 0;
var gecko = ua.indexOf("GECKO",0) >= 0;
var geckoNN = (gecko && ua.indexOf("NETSCAPE",0) >= 0);
var firefox = (gecko && ua.indexOf("FIREFOX",0) >= 0);
var safari = ua.indexOf("SAFARI",0) >= 0;
var opera = window.opera;
var apvernum = parseInt(apver);
//var ver = parseInt(navigator.appVersion); // ex. 3

var nn4  = ((nn && apvernum <= 4));
var winIE = ((windows && ua.indexOf('MSIE',0) >= 0));
var winIE55 = ((windows && ua.indexOf('MSIE 5.5',0) >= 0));
var macIE5 = ((mac && ua.indexOf('MSIE 5.',0) >= 0));

var sv = (function(){
if(!safari) return false;
var ver = ua.split("/");
var vernum = ver[ver.length-1];
vernum = vernum.replace(/\./g,'');
vernum = vernum.slice(0,3);
var n = parseInt(vernum,10)
	if (n >= 412 && n < 522){
		return sv = "2";
	}else if (n >= 522) {
		return sv = "3";
	}
})();

if (!Array.prototype.forEach) {
  Array.prototype.forEach = function(fun /*, thisp*/) {
    var len = this.length;
    if (typeof fun != "function") throw new TypeError();

    var thisp = arguments[1];
    for (var i = 0; i < len; i++){
      if (i in this) fun.call(thisp, this[i], i, this);
    }
  };
}

/* preload */
/* preload image */
var impath="http://www.mitsubishi-motors.co.jp/share/images/";


document.write('<meta http-equiv="Imagetoolbar" content="no">');


function preloadimg(imgnm) {
	if (document.images) {
		imgnmimg = new Image();
		imgnmimg.src = imgnm;	
	}
}

function preloadimgArray(arraynm) {
	if (document.images) {
		for (i=0; i<arraynm.length; i++) {
			(new Image()).src = arraynm[i];
		}
	}
}


/* swap */
function swapimage(nm,img) {
	document.images[nm].src = img;
}

function swapimageArray(nm,img) {
	var tmpNM=nm.split("|");
	var tmpI=img.split("|");
	for (i=0; i<tmpNM.length; i++) {
		document.images[tmpNM[i]].src = tmpI[i];
	}
}

/* window open */
function openWin(url,title,wdh,hgt,opt) {
	var win;
	if (url) {
		if (!title) title = "_blank";
		if (!opt) {
			var opti = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,favorites=yes";
		} else if (opt=="1") {
			var opti = "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,resizable=yes,favorites=no";
		} else if (opt=="2") {
			var opti = "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,favorites=no";
		} else if (opt=="3") {
			var opti = "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no,favorites=no";
		} else if (opt=="4") {
			<!-- MMTV -->
			var opti = "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,favorites=no";
			wdh = "950";
			hgt = "635";
		} else if (opt=="5") {
			<!-- 外部サイトリンク -->
			var opti = "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,resizable=yes,favorites=no";
			wdh = "595";
			hgt = "476";
		} else if (opt=="6") {
			<!-- oneline Simu -->
			var opti = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,favorites=no,width=850,height=620,top=30,left=10";
			//wdh = "595";
			//hgt = "476";
		} 
		
		if(!!wdh&&!!hgt) opti+=",width="+wdh+",height="+hgt;
		win = window.open(url,title,opti);
		win.focus();
	}
}

/* fullscreen window open */
function openFullWin(url,title,opt) {
 	var win;
	if (!!window && url) {
		if (!title) title = "_blank";
		if (!opt) {
			opt = "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,resizable=yes,favorites=no";
			//opt = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,favorites=yes";
		}
		var wdh=screen.availWidth;
		var hgt=screen.availHeight;
		if (windows && ie){
			opt="fullscreen=yes";
					win = window.open(url,title,opt);
					return;
		}
		else if (mac){
			if (safari){
				wdh=screen.width;
				hgt=screen.height;
			}else if(ie){
				wdh=screen.width;
				hgt=screen.height;
				opt+=",left=0,top=0";
				win = window.open(url,title,opt);
				win.resizeTo(wdh,hgt);
				return;
			}else{
				wdh=wdh-10;
				hgt=hgt-28;
				opt+=",width="+wdh+",height="+hgt;
			}
		}else{
			wdh=wdh-10;
			hgt=hgt-28;
			opt+=",width="+wdh+",height="+hgt;
		}
		win = window.open(url,title,opt);
		win.moveTo(0,0);
		win.focus();
	}
}

function downprintwrite(pname){

var downbtwrite = '';
downbtwrite += '<div class="'+pname+'">';
downbtwrite += '<p><a href="#" onClick="window.print();" onKeyPress="window.print();"><img src="/share/images/cmn_im_02.gif" alt="" width="16" height="10">このページを印刷する<\/a>';
downbtwrite += '<\/p><\/div><!-- /printBox -->';
if(!macIE5){
document.write(downbtwrite);
}
}


//parent window Link
function parentwinlink(url){
window.opener.location.href = url;
opener.focus();
}

//page print
function pgprint(){
if(!macIE5){
window.print();
}
}

//event Listener
function addListener(target, type, func) {
  if(target.attachEvent) {
    target.attachEvent("on" + type, function() {func.call(target, window.event);});
  } else if(target.addEventListener) {
    target.addEventListener(type, func, false);
  } else {
    target["on" + type] = func;
  }
}

function remListener(target, type, func) {
  if(target.attachEvent) {
    target.detachEvent("on" + type, func);
  } else if(target.addEventListener) {
    target.removeEventListener(type, func, false);
  }
}

//cookie

var cookieobj = {};

cookieobj.setcookie  = function(setdata,setname,setdate){
	var xDay = new Date;
	xDay.setDate(xDay.getDate() + setdate); // 有効期限
	xDay = xDay.toGMTString();  //GMT形式の文字列に変換
	document.cookie = setname + "=" +escape(setdata) + ";expires=" + xDay;
}

cookieobj.getcookie  = function(getname){

   myCookie = getname + "=";
   myValue = null;
   myStr = document.cookie + ";" ;
   myOfst = myStr.indexOf(myCookie);
   if (myOfst != -1){
      myStart = myOfst + myCookie.length;
      myEnd   = myStr.indexOf(";" , myStart);
      myValue = unescape(myStr.substring(myStart,myEnd));
   }
   return myValue;
   
}

	///////////////
	var __chkbox = location.href;
	if(__chkbox.indexOf("mitsubishi-motors.co.jp") != -1 || __chkbox.indexOf("mitsubishi-motors.com") != -1){
	var nv_root_path = "http://www.mitsubishi-motors.co.jp";
	}else{
	var nv_root_path = "";
	}
	///////////////
	
//var nv_root_path = 'http://www.mitsubishi-motors.co.jp';
//var nv_root_path = '';

	
	
	
/////////////////////////////////

var DOMready = {};
DOMready.tgBox = new Array();
var flagbox = false;
var loadbox = false;

DOMready.tgFunc = function(tgfun){

	if(loadbox){
	tgfun.call();
	}else{
	DOMready.tgBox.push(tgfun);
	if(!flagbox) DOMready.domFunc();
}
}

DOMready.domFunc = function() {

flagbox = true;

if(firefox){
	addListener(window,"DOMContentLoaded",DOMready.comp);
}else if(winIE && window == top){
	try {
		document.documentElement.doScroll("left");
	} catch(error){
		setTimeout(arguments.callee,0);
		return;
	}
	DOMready.comp();
}else if(safari){	
	if(document.readyState != "loaded" && document.readyState != "complete" ) {
		setTimeout(arguments.callee,0);
		return;
	}
	var stnum = document.getElementsByTagName('style').length;
	var linkstyle = document.getElementsByTagName('link');
	for(var d=0;d<linkstyle.length;d++){
	if(linkstyle[d].rel == 'stylesheet'){
		stnum++;
	}
	}
	if (document.styleSheets.length != stnum) {
		setTimeout(arguments.callee,0);
		return;
	}
	DOMready.comp();
}else{
	addListener(window, "load", DOMready.comp);
}
}

DOMready.comp = function(){
	for(var k=0;k<DOMready.tgBox.length;k++){
		DOMready.tgBox[k].call();
	}
	DOMready.tgBox = null;
}
	
DOMready.flag = function(){
	loadbox = true;
}

addListener(window, "load", DOMready.flag);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//   roll over   //
//////////////////
// ロールオーバーさせたい imgタグに class="imageover"
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function preimgfunc(objsrc){
	var ftype = objsrc.substring(objsrc.lastIndexOf("."), objsrc.length);
	var presrc = objsrc.replace(ftype, "_a"+ftype);
	return presrc;
}

var setimgover = function (){
	if(document.getElementById("car_contents") || document.getElementById("popmessage")) return;
	
	if(document.getElementById("general")){
		var imgtag = document.getElementById("general").getElementsByTagName("img");
	}else if(document.getElementById("pop")){
		var imgtag = document.getElementById("pop").getElementsByTagName("img");
	}else{
		return;	
	}
	
	var fieldimgnum = imgtag.length;
	var prefimg = new Array();
	
	for(var i=0; i<fieldimgnum;i++){
	var classnm  = imgtag[i].className.split(" ");
		for(var z=0;z<classnm.length;z++){
			if(classnm[z] == "imageover"){

				var fimg = imgtag[i].src; 
				prefimg[i] = new Image();
    			prefimg[i].src = preimgfunc(fimg);
				var imgoverObj = new imgoverfunc(imgtag[i],prefimg[i]);
				imgoverObj.evset();
			}
		}
	}
}


var imgoverfunc = function(imgelm,oversrc){
	this.initialize.apply(this, arguments);
}
imgoverfunc.prototype = {
	
	initialize : function(imgelm,oversrc){
		this.nowelm = imgelm;
		this.oversrc = oversrc;
		this.basesrc = this.nowelm.src;
	},
	
	evset : function(){
		var funcover = (function(imgoverfunc){
		return function(e){
			this.src = imgoverfunc.oversrc.src;
		}
		})(this);
		
		var funcout = (function(imgoverfunc){
		return function(e){
			this.src = imgoverfunc.basesrc;
		}
		})(this);

		addListener(this.nowelm, "mouseover", funcover);
		addListener(this.nowelm, "mouseout", funcout);
		addListener(this.nowelm, "click", funcout);
	
	}
}

DOMready.tgFunc(setimgover);
//addListener(window, 'load', setimgover);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
function stopbubble(e){	
	if (e.target) { 
     e.stopPropagation(); 
	 e.preventDefault(); 
	}else if (window.event.srcElement) { 
     window.event.cancelBubble = true; 
	window.event.returnValue=false;
   	}  	
}

