
function CarReqData(url,imgsrc,imgalt,blocktxt){
	this.url = url;
	this.imgsrc = imgsrc;
	this.imgalt = imgalt;
	this.blocktxt = blocktxt;
}

var car_reqnavi = new Array(
new CarReqData('href="/purchase/onlinequote/index.html"','/purchase/images/index_tx_09.gif','オンライン見積もり','ご希望の車種のお見積もりを計算できます。'),
new CarReqData('href="http://map.mitsubishi-motors.co.jp/index.html"','/purchase/images/index_tx_01.gif','販売店検索','三菱自動車の販売店（ディーラー・サテライトショップ）を検索することができます。'),
new CarReqData('href="http://democar.mitsubishi-motors.co.jp/democar/carSelect.html"','/purchase/images/index_tx_02.gif','展示車/試乗車検索','ご試乗またはご覧になりたいクルマを取り扱う販売店（ディーラー）を検索することができます。'),
new CarReqData('href="https://customer.mitsubishi-motors.co.jp/olc/OlcTop.do\?sourceId=OLC" target="_blank"','/purchase/images/index_tx_03.gif','カタログ請求','ご希望の車種のカタログを請求できます。'),
new CarReqData('href="/purchase/cataloglist/index.html"','/purchase/images/index_tx_04.gif','カタログ一覧','ご希望の車種のカタログを、データ（PDF形式）でご覧いただけます。'),
new CarReqData('href="/reference/index.html"','/purchase/images/index_tx_13.gif','FAQコーナー','お問い合わせの多いご質問をＦＡＱとして掲載しております。CM情報や車名の由来などをご確認頂けます。')
);

var nvnum = car_reqnavi.length;


function carReq(){
var i;

if(nvnum > 0){
	
for(i=1;i<=nvnum;i++){
var reqbox = '';
var idf = i%2;	
var texbox = 'textBlock';

	if(idf != 0){
	var clbox = 'left';
	}else{
	var clbox = 'right';
	}
document.writeln('<div class="'+clbox+'">');
reqbox += '<p><a '+car_reqnavi[i-1].url+' class="imgLink"><img src="'+car_reqnavi[i-1].imgsrc+'" alt="'+car_reqnavi[i-1].imgalt+'" width="274" height="25"></a></p>\n';
reqbox += '<div class="'+texbox+'"><p>'+car_reqnavi[i-1].blocktxt+'<\/p><\/div>\n';
document.write(reqbox);
document.writeln('<\/div>');
	
	if(idf == 0 && i != nvnum){
		document.writeln('<br class="clear">');
	}
}
document.writeln('<br class="clear">');
}
}